import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BoxViewComponent} from './box-view.component';

const routes: Routes = [
  {
    path: '',
    component: BoxViewComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class BoxViewRoutingModule {
}
