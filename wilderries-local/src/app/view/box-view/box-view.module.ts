import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {BoxViewRoutingModule} from './box-view-routing.module';
import {BoxViewComponent} from './box-view.component';
import {BoxModule} from '../box/box.module';
import {ReturnBoxModule} from '../return-box/return-box.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    BoxViewRoutingModule,
    BoxModule,
    ReturnBoxModule
  ],
  declarations: [
    BoxViewComponent,
  ],
  exports: [
    BoxViewComponent
  ]
})
export class BoxViewModule {
}

