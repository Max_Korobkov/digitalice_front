import {Component} from '@angular/core';

@Component({
  selector: 'box-view',
  templateUrl: './box-view.component.html'
})
export class BoxViewComponent {
  public isShowCommon = false;
  public isShowReturn = false;

  constructor() {}

  UpdateShowingPage(page) {
    this.isShowCommon = false;
    this.isShowReturn = false;
  }
}
