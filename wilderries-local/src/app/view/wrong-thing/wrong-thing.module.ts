import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {OrdersService, ReturnBoxesService} from '../../services/common';
import {WrongThingRoutingModule} from './wrong-thing-routing.module';
import {WrongThingListComponent} from './list/wrong-thing-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    WrongThingRoutingModule
  ],
  declarations: [
    WrongThingListComponent,
  ],
  exports: [
    WrongThingListComponent
  ],
  providers: [
    CuiModelHelper,
    ReturnBoxesService
  ]
})
export class WrongThingModule {
}

