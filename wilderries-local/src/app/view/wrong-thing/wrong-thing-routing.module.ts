import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WrongThingListComponent} from './list/wrong-thing-list.component';

const routes: Routes = [
  {
    path: '',
    // pathMatch: 'full',
    component: WrongThingListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class WrongThingRoutingModule {
}
