import {Component} from '@angular/core';
import {ReturnBox, ReturnBoxesService} from '../../../services/common';
import {MapResponseToListAndSummaryData} from 'cui-controls/cui-data';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'wrong-thing-list',
  templateUrl: './wrong-thing-list.component.html'
})
export class WrongThingListComponent {
  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Ошибочные вещи',
    titleLevel: 1,
    properties: [
      {
        label: 'ID коробки',
        value: (item: ReturnBox) => item.id
      },
      {
        label: 'Статус',
        value: (item: ReturnBox) => item.status
      },
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .listReturnBoxes()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => of({status: 'Новая коробка'}),
    saveFunction: (item: ReturnBox) => {
      return item.id
        ? this.service.putReturnBox(item.id, item)
        : this.service.postreturnBoxesReturnBoxId(item.id, item);
    },
    deleteFunction: (item: ReturnBox) => this.service.deleteReturnBox(item.id)
  };

  constructor(private service: ReturnBoxesService) {}
}
