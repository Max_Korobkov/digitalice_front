import {Component} from '@angular/core';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';
import {ReturnBox, ReturnBoxesService} from '../../../services/common';
import {BoxListExpandComponent} from '../../box/list/expand/box-list-expand.component';
import {ReturnBoxListExpandComponent} from './expand/return-box-list-expand.component';

@Component({
  selector: 'return-box-list',
  templateUrl: './return-box-list.component.html'
})
export class ReturnBoxListComponent {
  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Коробки возврата',
    titleLevel: 1,
    properties: [
      {
        label: 'ID коробки',
        value: (item: ReturnBox) => item.id
      },
      {
        label: 'Статус',
        value: (item: ReturnBox) => item.status
      },
      {
        label: 'Изменено',
        value: (item: ReturnBox) => item.is_changed ? 'Да' : 'Нет'
      }
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .listReturnBoxes()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => of({status: 'Новая коробка'}),
    saveFunction: (item: ReturnBox) => {
      return item.id
        ? this.service.putReturnBox(item.id, item)
        : this.service.postreturnBoxesReturnBoxId(item.id, item);
    },
    deleteFunction: (item: ReturnBox) => this.service.deleteReturnBox(item.id),
    expandView: {
      component: ReturnBoxListExpandComponent,
      data: () => {
        return {};
      },
    },
  };

  constructor(private service: ReturnBoxesService) {}
}
