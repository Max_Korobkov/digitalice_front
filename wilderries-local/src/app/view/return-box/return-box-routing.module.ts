import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReturnBoxListComponent} from './list/return-box-list.component';

const routes: Routes = [
  {
    path: '',
    // pathMatch: 'full',
    component: ReturnBoxListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class ReturnBoxRoutingModule {
}
