import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {ReturnBoxListComponent} from './list/return-box-list.component';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {ReturnBoxesService} from '../../services/common';
import {ReturnBoxRoutingModule} from './return-box-routing.module';
import {ReturnBoxListExpandComponent} from './list/expand/return-box-list-expand.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    ReturnBoxRoutingModule
  ],
  declarations: [
    ReturnBoxListComponent,
    ReturnBoxListExpandComponent
  ],
  exports: [
    ReturnBoxListComponent,
    ReturnBoxListExpandComponent
  ],
  providers: [
    CuiModelHelper,
    ReturnBoxesService
  ],
  entryComponents: [
    ReturnBoxListExpandComponent
  ]
})
export class ReturnBoxModule {
}

