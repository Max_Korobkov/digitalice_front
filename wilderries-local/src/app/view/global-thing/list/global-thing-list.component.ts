import {Component} from '@angular/core';
import {GlobalThing, GlobalThingsService, ReturnBox, ReturnBoxesService} from '../../../services/common';
import {MapResponseToListAndSummaryData} from 'cui-controls/cui-data';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'global-thing-list',
  templateUrl: './global-thing-list.component.html'
})
export class GlobalThingListComponent {
  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Товары на сайте',
    titleLevel: 1,
    properties: [
      {
        label: 'ID вещи',
        value: (item: GlobalThing) => item.id
      },
      {
        label: 'Наименование',
        value: (item: GlobalThing) => item.name
      },
      {
        label: 'Описание',
        value: (item: GlobalThing) => item.description
      },
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .getGlobalThings()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => of({status: 'Новая коробка'}),
    saveFunction: (item: ReturnBox) => {
      return item.id
        ? this.service.putShopGlobalThing(item.id, item)
        : this.service.postGlobalThingsGlobalThingId(item.id, item);
    },
    deleteFunction: (item: ReturnBox) => this.service.deleteShopGlobalThing(item.id)
  };

  constructor(private service: GlobalThingsService) {}
}
