import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GlobalThingListComponent} from './list/global-thing-list.component';

const routes: Routes = [
  {
    path: '',
    // pathMatch: 'full',
    component: GlobalThingListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class GlobalThingRoutingModule {
}
