import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {GlobalThingsService} from '../../services/common';
import {GlobalThingListComponent} from './list/global-thing-list.component';
import {ReturnRoutingModule} from '../return/return-routing.module';
import {GlobalThingRoutingModule} from './global-thing-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    GlobalThingRoutingModule
  ],
  declarations: [
    GlobalThingListComponent,
  ],
  exports: [
    GlobalThingListComponent
  ],
  providers: [
    CuiModelHelper,
    GlobalThingsService
  ]
})
export class GlobalThingModule {
}

