import {Component} from '@angular/core';
import {Buyer, BuyersService, ReturnBox, ReturnBoxesService} from '../../../services/common';
import {MapResponseToListAndSummaryData} from 'cui-controls/cui-data';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'buyer-list',
  templateUrl: './buyer-list.component.html'
})
export class BuyerListComponent {
  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Покупатели',
    titleLevel: 1,
    properties: [
      {
        label: 'ID покупателя',
        value: (item: Buyer) => item.id
      },
      {
        label: 'ФИО',
        value: (item: Buyer) => item.second_name + ' ' + item.first_name + ' ' + item.patronymic
      },
      {
        label: 'Номер телефона',
        value: (item: Buyer) => item.phone
      },
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .getBuyers()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => of({status: 'Новая коробка'}),
    saveFunction: (item: ReturnBox) => {
      return item.id
        ? this.service.putBuyer(item.id, item)
        : this.service.postBuyerId(item.id, item);
    },
    deleteFunction: (item: ReturnBox) => this.service.deleteBuyer(item.id)
  };

  constructor(private service: BuyersService) {}
}
