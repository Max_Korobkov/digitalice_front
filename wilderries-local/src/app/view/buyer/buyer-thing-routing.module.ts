import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BuyerListComponent} from './list/buyer-list.component';

const routes: Routes = [
  {
    path: '',
    // pathMatch: 'full',
    component: BuyerListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class BuyerThingRoutingModule {
}
