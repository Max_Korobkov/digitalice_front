import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {BuyersService} from '../../services/common';
import {BuyerListComponent} from './list/buyer-list.component';
import {BuyerThingRoutingModule} from './buyer-thing-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    BuyerThingRoutingModule
  ],
  declarations: [
    BuyerListComponent,
  ],
  exports: [
    BuyerListComponent
  ],
  providers: [
    CuiModelHelper,
    BuyersService
  ]
})
export class BuyerThingModule {
}

