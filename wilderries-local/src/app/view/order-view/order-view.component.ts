import {Component} from '@angular/core';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';
import {OrderedThing, OrderThingsService} from '../../services/common';

@Component({
  selector: 'order-view',
  templateUrl: './order-view.component.html'
})
export class OrderViewComponent {
  public isShowOrders = false;
  public isShowOrderItems = false;
  public isShowWrongItems = false;
  public isShowReturnItems = false;

  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Заказанные вещи',
    titleLevel: 1,
    properties: [
      {
        label: 'ID заказанной вещи',
        value: (item: OrderedThing) => item.id
      },
      {
        label: 'Синхронизировано с сервером',
        value: (item: OrderedThing) => item.is_confirmed
      },
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .listOrderThings()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => of({status: 'Новая коробка'}),
    saveFunction: (item: OrderedThing) => {
      return item.id
        ? this.service.putOrderThing(item.id, item)
        : this.service.postorderedThingsOrderedThingId(item.id, item);
    },
    deleteFunction: (item: OrderedThing) => this.service.deleteOrderThing(item.id)
  };

  constructor(private service: OrderThingsService) {}

  UpdateShowingPage(page) {
    this.isShowOrders = false;
    this.isShowOrderItems = false;
    this.isShowWrongItems = false;
    this.isShowReturnItems = false;
  }
}
