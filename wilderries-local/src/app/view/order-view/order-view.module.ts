import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {OrderThingsService} from '../../services/common';
import {OrderViewRoutingModule} from './order-view-routing.module';
import {OrderViewComponent} from './order-view.component';
import {OrderThingModule} from '../order-thing/order-thing.module';
import {WrongThingModule} from '../wrong-thing/wrong-thing.module';
import {OrderModule} from '../order/order.module';
import {ReturnModule} from '../return/return.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    OrderViewRoutingModule,
    OrderThingModule,
    WrongThingModule,
    OrderModule,
    ReturnModule
  ],
  declarations: [
    OrderViewComponent,
  ],
  exports: [
    OrderViewComponent
  ]
})
export class OrderViewModule {
}

