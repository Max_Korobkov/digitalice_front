import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CellListComponent} from './list/cell-list.component';

const routes: Routes = [
  {
    path: '',
    // pathMatch: 'full',
    component: CellListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class CellRoutingModule {
}
