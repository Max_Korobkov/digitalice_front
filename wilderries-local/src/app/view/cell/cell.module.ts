import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {OrdersService, ReturnBoxesService} from '../../services/common';
import {CellListComponent} from './list/cell-list.component';
import {CellRoutingModule} from './cell-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    CellRoutingModule
  ],
  declarations: [
    CellListComponent,
  ],
  exports: [
    CellListComponent
  ],
  providers: [
    CuiModelHelper,
    ReturnBoxesService
  ]
})
export class CellModule {
}

