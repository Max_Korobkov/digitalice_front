import {Component} from '@angular/core';
import {Cell, CellsService, ReturnBox, ReturnBoxesService} from '../../../services/common';
import {MapResponseToListAndSummaryData} from 'cui-controls/cui-data';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'cell-list',
  templateUrl: './cell-list.component.html'
})
export class CellListComponent {
  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Ячейка',
    titleLevel: 1,
    properties: [
      {
        label: 'ID ячейки',
        value: (item: Cell) => item.id
      },
      {
        label: 'ID заказа',
        value: (item: Cell) => item.order_id
      },
      {
        label: 'Заполнена',
        value: (item: Cell) => item.is_full
      },
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .listCells()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => of({status: 'Новая коробка'}),
    saveFunction: (item: ReturnBox) => {
      return item.id
        ? this.service.putCell(item.id, item)
        : this.service.postCellsCellId(item.id, item);
    },
    deleteFunction: (item: ReturnBox) => this.service.deleteCell(item.id)
  };

  constructor(private service: CellsService) {}
}
