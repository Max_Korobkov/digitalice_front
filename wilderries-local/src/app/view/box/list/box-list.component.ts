import {Component} from '@angular/core';
import {Box, BoxesService, ReturnBox, ReturnBoxesService} from '../../../services/common';
import {MapResponseToListAndSummaryData} from 'cui-controls/cui-data';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';
import {BoxListExpandComponent} from './expand/box-list-expand.component';

@Component({
  selector: 'box-list',
  templateUrl: './box-list.component.html'
})
export class BoxListComponent {
  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Коробки доставки',
    titleLevel: 1,
    properties: [
      {
        label: 'ID коробки',
        value: (item: Box) => item.id
      },
      {
        label: 'ID ПВЗ',
        value: (item: Box) => item.destination_id
      },
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .getBoxes()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => of({status: 'Новая коробка'}),
    saveFunction: (item: ReturnBox) => {
      return item.id
        ? this.service.putShopBoxId(item.id, item)
        : this.service.postboxesBoxId(item.id, item);
    },
    deleteFunction: (item: ReturnBox) => this.service.deleteShopBoxId(item.id),
    expandView: {
      component: BoxListExpandComponent,
      data: () => {
        return {};
      },
    },
  };

  constructor(private service: BoxesService) {}
}
