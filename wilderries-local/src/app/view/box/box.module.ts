import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {BoxesService} from '../../services/common';
import {BoxListComponent} from './list/box-list.component';
import {BoxRoutingModule} from './box-routing.module';
import {BoxListExpandComponent} from './list/expand/box-list-expand.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    BoxRoutingModule
  ],
  declarations: [
    BoxListComponent,
    BoxListExpandComponent
  ],
  exports: [
    BoxListComponent,
    BoxListExpandComponent
  ],
  providers: [
    CuiModelHelper,
    BoxesService
  ],
  entryComponents: [
    BoxListExpandComponent
  ]
})
export class BoxModule {
}

