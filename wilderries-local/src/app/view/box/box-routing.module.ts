import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BoxListComponent} from './list/box-list.component';

const routes: Routes = [
  {
    path: '',
    // pathMatch: 'full',
    component: BoxListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class BoxRoutingModule {
}
