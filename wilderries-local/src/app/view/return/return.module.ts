import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {ReturnBoxesService, ReturnsService} from '../../services/common';
import {ReturnListComponent} from './list/return-list.component';
import {ReturnRoutingModule} from './return-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    ReturnRoutingModule
  ],
  declarations: [
    ReturnListComponent,
  ],
  exports: [
    ReturnListComponent
  ],
  providers: [
    CuiModelHelper,
    ReturnsService
  ]
})
export class ReturnModule {
}

