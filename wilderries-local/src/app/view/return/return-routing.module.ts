import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReturnListComponent} from './list/return-list.component';

const routes: Routes = [
  {
    path: '',
    // pathMatch: 'full',
    component: ReturnListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class ReturnRoutingModule {
}
