import {Component} from '@angular/core';
import {ReturnBox, ReturnBoxesService, ReturnsService} from '../../../services/common';
import {MapResponseToListAndSummaryData} from 'cui-controls/cui-data';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'return-list',
  templateUrl: './return-list.component.html'
})
export class ReturnListComponent {
  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Возвраты',
    titleLevel: 1,
    properties: [
      {
        label: 'ID возврата',
        value: (item: ReturnBox) => item.id
      },
      {
        label: 'Статус',
        value: (item: ReturnBox) => item.status
      },
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .listReturns()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => of({status: 'Новая коробка'}),
    saveFunction: (item: ReturnBox) => {
      return item.id
        ? this.service.putReturn(item.id, item)
        : this.service.postreturnsReturnId(item.id, item);
    },
    deleteFunction: (item: ReturnBox) => this.service.deleteReturn(item.id)
  };

  constructor(private service: ReturnsService) {}
}
