import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {OrderThingsService} from '../../services/common';
import {OrderThingListComponent} from './list/order-thing-list.component';
import {OrderThingRoutingModule} from './order-thing-routing.module';
import {OrderThingListNewComponent} from './list/new/order-thing-list-new.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    OrderThingRoutingModule
  ],
  declarations: [
    OrderThingListComponent,
    OrderThingListNewComponent
  ],
  exports: [
    OrderThingListComponent,
    OrderThingListNewComponent
  ],
  providers: [
    CuiModelHelper,
    OrderThingsService
  ],
  entryComponents: [
    OrderThingListNewComponent
  ]
})
export class OrderThingModule {
}

