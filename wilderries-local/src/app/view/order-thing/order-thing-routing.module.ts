import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {OrderThingListComponent} from './list/order-thing-list.component';

const routes: Routes = [
  {
    path: '',
    // pathMatch: 'full',
    component: OrderThingListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class OrderThingRoutingModule {
}
