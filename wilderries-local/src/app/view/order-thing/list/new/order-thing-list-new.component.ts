import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'order-thing-list-new',
  templateUrl: './order-thing-list-new.component.html',
})
export class OrderThingListNewComponent {
  @Input() public modalRef: BsModalRef;

  @Output() public createdNewSensor: EventEmitter<any> = new EventEmitter<any>();

  public newSensor: any = {};

  HideModal() {
    this.modalRef.hide()
  }

  AddSensor() {
    this.createdNewSensor.emit(this.newSensor);
  }
}
