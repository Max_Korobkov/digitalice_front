import {Component} from '@angular/core';
import {OrderedThing, OrderThingsService, ReturnBox, ReturnBoxesService} from '../../../services/common';
import {MapResponseToListAndSummaryData} from 'cui-controls/cui-data';
import {map, share} from 'rxjs/operators';
import {of} from 'rxjs';
import {OrderThingListNewComponent} from './new/order-thing-list-new.component';
import {BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'order-thing-list',
  templateUrl: './order-thing-list.component.html'
})
export class OrderThingListComponent {
  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Заказанные вещи',
    titleLevel: 1,
    properties: [
      {
        label: 'ID заказанной вещи',
        value: (item: OrderedThing) => item.id
      },
      {
        label: 'ID вещи на сайте',
        value: (item: OrderedThing) => item.global_thing_id
      },
      {
        label: 'ID заказа',
        value: (item: OrderedThing) => item.order_id
      },
      {
        label: 'ID коробки доставки',
        value: (item: OrderedThing) => item.box_id
      },
      {
        label: 'Статус',
        value: (item: OrderedThing) => item.status
      },
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .listOrderThings()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => {
      const modalRef = this.modalService.show(OrderThingListNewComponent);
      // (<OrderThingListNewComponent>modalRef.content). = this.organisationId;

      const obs = (<OrderThingListNewComponent>modalRef.content).createdNewSensor;

      obs.pipe(share()).subscribe(
        () => modalRef.hide()
      );
      console.log(obs);

      return obs;
    },
    saveFunction: (item: OrderedThing) => {
      return item.id
        ? this.service.postorderedThingsOrderedThingId(item.id, item)
        : this.service.postorderedThingsOrderedThingId(item.id, item);
    },
    deleteFunction: (item: ReturnBox) => this.service.deleteOrderThing(item.id)
  };

  constructor(private service: OrderThingsService,
              private modalService: BsModalService) {}
}
