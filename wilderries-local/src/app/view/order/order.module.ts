import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuiControlsModule} from 'cui-controls/cui-controls.module';
import {FormsModule} from '@angular/forms';
import {OrderListComponent} from './list/order-list.component';
import {CuiModelHelper} from 'cui-controls/services/cui/cui.helper';
import {OrdersService} from '../../services/common';
import {OrderRoutingModule} from './order-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CuiControlsModule,
    OrderRoutingModule
  ],
  declarations: [
    OrderListComponent,
  ],
  exports: [
    OrderListComponent
  ],
  providers: [
    CuiModelHelper,
    OrdersService
  ]
})
export class OrderModule {
}

