import {Component} from '@angular/core';
import {Order, OrdersService} from '../../../services/common';
import {MapResponseToListAndSummaryData} from 'cui-controls/cui-data';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'order-list',
  templateUrl: './order-list.component.html'
})
export class OrderListComponent {
  public config = {
    enableAutoSave: true,
    saveAfterAddNew: true,
    title: 'Заказы',
    titleLevel: 1,
    properties: [
      {
        label: 'ID заказа',
        value: (item: Order) => item.id
      },
      {
        label: 'Скомплектован',
        value: (item: Order) => item.is_complected
      },
    ],
    spawnData: (searchString?: string, page?: number, pageSize?: number) => {
      return this.service
        .listOrders()
        .pipe(
          map((item) => {return {list: item}})
        )
    },
    newItem: () => of({status: 'Новая коробка'}),
    saveFunction: (item: Order) => {
      return item.id
        ? this.service.putOrder(item.id, item)
        : this.service.postordersOrderId(item.id, item);
    },
    deleteFunction: (item: Order) => this.service.deleteOrder(item.id)
  };


  constructor(private service: OrdersService) {}
}
