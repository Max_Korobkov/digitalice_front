import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { BoxesService } from './api/boxes.service';
import { BuyersService } from './api/buyers.service';
import { CellsService } from './api/cells.service';
import { GlobalThingsService } from './api/globalThings.service';
import { OrderThingsService } from './api/orderThings.service';
import { OrdersService } from './api/orders.service';
import { ReturnBoxesService } from './api/returnBoxes.service';
import { ReturnsService } from './api/returns.service';
import { WrongThingsService } from './api/wrongThings.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    BoxesService,
    BuyersService,
    CellsService,
    GlobalThingsService,
    OrderThingsService,
    OrdersService,
    ReturnBoxesService,
    ReturnsService,
    WrongThingsService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
